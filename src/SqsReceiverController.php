<?php
namespace App;

class SqsReceiverController extends AwsController
{
    function __construct() {
        header("Content-Type: text/html");
        parent::__construct();
    }

    public function action() {
       // Receiving Sqs
       echo "Receiving SQS \n";
        try {
        $result = $this->client->receiveMessage(array(
            'AttributeNames' => ['SentTimestamp'],
            'MaxNumberOfMessages' => 2,
            'MessageAttributeNames' => ['All'],
            'QueueUrl' => parent::QueueUri, // REQUIRED
            'WaitTimeSeconds' => 0,
        ));
        if (count($result->get('Messages')) > 0) {
            var_dump($result->get('Messages')[0]);
            $result = $this->client->deleteMessage([
                'QueueUrl' => parent::QueueUri, // REQUIRED
                'ReceiptHandle' => $result->get('Messages')[0]['ReceiptHandle'] // REQUIRED
            ]); 
        } else {
            echo "No messages in queue. \n";
        } 
        } catch (AwsException $e) {
        // output error message if fails
        error_log($e->getMessage());
        }
    }
}