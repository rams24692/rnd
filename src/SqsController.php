<?php
namespace App;

class SqsController extends AwsController
{
    function __construct() {
        header("Content-Type: text/html");
        parent::__construct();
    }
    public function action() {
        
        //sending message
        echo "Sending SQS...............\n";
        $message = [
            'DelaySeconds' => 10,
            'MessageAttributes' => [
                "Title" => [
                    'DataType' => "String",
                    'StringValue' => "The Hitchhiker's Guide to the Galaxy"
                ],
                "Author" => [
                    'DataType' => "String",
                    'StringValue' => "Douglas Adams."
                ],
                "WeeksOn" => [
                    'DataType' => "Number",
                    'StringValue' => "6"
                ]
            ],
            'MessageBody' => "Hey This is Rams.",
            'QueueUrl' => parent::QueueUri
        ];
        
        try {
            $result = $this->client->sendMessage($message);
            var_dump($result);
        } catch (AwsException $e) {
            error_log($e->getMessage());
        }
        
    }

}