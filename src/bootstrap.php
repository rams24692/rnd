<?php

$uri = $_SERVER['REQUEST_URI'];
$uri = str_replace("/rnd",'',$uri);
$ctrl = null;
if($uri == '/') {
    $ctrl = new \App\DefaultController();
} else  {
    $uri_parts = explode("/", $uri);
    $class = "\\App\\".ucwords($uri_parts[1]).'Controller';
    $ctrl = new $class();
}
if($ctrl === null) {
    throw new Exception('Undefind action');
}
$ctrl->action();