<?php
namespace App;

use Aws\Sqs\SqsClient; 
use Aws\Exception\AwsException;

class AwsController {
    const QueueUri = 'https://sqs.us-east-2.amazonaws.com/542439830978/usac-dev	';
    protected $client = null;
    function __construct() {
        $this->client = new SqsClient([
            'profile' => 'default',
            'region' => 'us-east-2',
            'version' => 'latest'
            
        ]);
    }
}